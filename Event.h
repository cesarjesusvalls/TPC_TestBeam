#include <vector> 

class WaveForm {
 public: 
  short DCC;
  short FEM;
  short FEC;
  short ASIC;
  short ElCh;
  int Channel;
  int Column;
  int Row;
  bool compress;
  std::vector<int> ADC;
  std::vector<int> Time;
};


class Event{
 public:
  int eventnumber;
  bool compress; 
  std::vector<WaveForm> Pulse;
};
