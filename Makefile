MIDASSYS=/Users/suvorov/SOFT/MIDAS/midas

ROOTINC=$(ROOTSYS)/include
MIDASINC=$(MIDASSYS)/include

ROOTANAINC=/Users/suvorov/SOFT/MIDAS/rootana/include
ROOTANALIB=/Users/suvorov/SOFT/MIDAS/rootana/lib

ROOTLIB=$(ROOTSYS)/lib
MIDASLIB=$(MIDASSYS)/lib

MIDASLIB=""
LIBS=""

UNAME = $(shell uname)
ifeq ($(UNAME), Linux)
	LIBS += -lrt -lROOTDataFrame -lROOTVecOps -pthread
	MIDASLIB=${MIDASSYS}/linux/lib/libmidas.a
else ifeq ($(UNAME), Darwin)
	MIDASLIB=${MIDASSYS}/darwin/lib/libmidas.a
endif

LIBS += -L/${ROOTANALIB} -lrootana -lutil -L${ROOTLIB} -L${ROOTLIB} ${MIDASLIB} -lGui -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -lm -ldl -rdynamic -lThread -lXMLParser -lXMLIO -lRHTTP -lm -lz -lpthread -Wl,-rpath,${ROOTLIB}

FLAGS=-Wall -Wuninitialized -DHAVE_LIBZ -DHAVE_ROOT -std=c++11 -m64 -DHAVE_ROOT_XML -DHAVE_ROOT_HTTP -DHAVE_THTTP_SERVER -DHAVE_MIDAS -DOS_LINUX -Dextname  -DHAVE_LIBNETDIRECTORY -DHAVE_XMLSERVER

all: geomManager.o mydict.o Decode.o RootConverter.o RootConverter.exe

geomManager.o: geomManager.cc
	g++ -c -o geomManager.o $(FLAGS) -I$(ROOTINC) geomManager.cc

mydict.o: mydict.C
	g++ -c -o mydict.o $(FLAGS) -I$(ROOTINC) mydict.C

Decode.o: Decode.cxx
	g++ -c -o Decode.o $(FLAGS) -I$(ROOTANAINC) -I$(ROOTINC) -I$(MIDASINC) Decode.cxx

RootConverter.o: RootConverter.cxx
	g++ -c -o RootConverter.o $(FLAGS) -I$(ROOTANAINC) -I$(ROOTINC) -I$(MIDASINC) RootConverter.cxx
RootConverter.exe:  mydict.o Decode.o geomManager.o RootConverter.o
	g++ -o RootConverter.exe -g RootConverter.o Decode.o geomManager.o mydict.o $(LIBS)

clean:
	@echo "Cleaning up..."
	@rm -rf *.o
	@rm -rf *.exe